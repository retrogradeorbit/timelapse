DEVICE = /dev/sde1
MNT = /mnt
FIRMWARE = firmware/a2300-100f-1.1.0-2955-full_ALPHA.zip

mount: 
	sudo mount $(DEVICE) $(MNT)

firmware: $(MNT)/CHDK

$(MNT)/CHDK:
	FW=`readlink -f $(FIRMWARE)`; \
	cd $(MNT) && sudo unzip $$FW

umount: 
	sudo umount $(DEVICE)

upload-scripts: firmware
	sudo cp -v scripts/* $(MNT)/CHDK/SCRIPTS/

download: .dlpath

.dlpath:
	python bin/make_unique_download_dir.py downloads > .dlpath
	@cat .dlpath
	mkdir -p `cat .dlpath`
	cp -av $(MNT)/DCIM/* `cat .dlpath`

	sudo rm -rf $(MNT)/DCIM/*

clean-flash:
	echo "Would you like to erase the flash drive $(DEVICE) [y/n]?"; \
	read ans; \
	if [ $$ans = y -o $$ans = Y ] ;\
	then echo "Deleting entire flash drive contents" ;\
	else exit 1 ;\
	fi	

	sudo rm -rf $(MNT)/*

clean-photos:
	echo "Would you like to erase the photos from flash drive $(DEVICE) [y/n]?"; \
	read ans; \
	if [ $$ans = y -o $$ans = Y ] ;\
	then echo "Deleting photos" ;\
	else exit 1 ;\
	fi	

	sudo rm -rf $(MNT)/DCIM/*

join: resize
	cd `cat .dlpath`; \
	ls -1tr resized/* | grep -v files.txt > files.txt ;\
	mencoder -nosound -noskip -oac copy -ovc copy -o output.avi -mf fps=30 'mf://@files.txt' ;\
	ffmpeg -i output.avi -y -sameq output-normal.avi

join-deflickered: .dlpath
	cd `cat .dlpath`; \
	ls -1tr resized/deflickered/* | grep -v files.txt > files.txt ;\
	mencoder -nosound -noskip -oac copy -ovc copy -o output.avi -mf fps=30 'mf://@files.txt' ;\
	ffmpeg -i output.avi -y -sameq output-deflickered.avi

resize: .dlpath
	cd `cat .dlpath`; \
	mkdir -p resized ; \
	mogrify -path resized -resize 1920x1080! */*.JPG

vp:
	virtualenv vp
	vp/bin/pip install unittest2 nose PIL

tests: vp
	vp/bin/nosetests bin/test_*.py

deflicker: vp
	vp/bin/python bin/deflicker.py `cat .dlpath`/resized/*
