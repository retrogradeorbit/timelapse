import unittest2 as unittest
from PIL import Image
from random import randint

import deflicker

class DeflickerTestSuite(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_image(self):
        self.assertIsNot(deflicker.get_image("testdata/image.jpg"), None)

    def test_average_rgb(self):
        self.assertEquals( (255.0, 255.0, 255.0),
                           deflicker.average_rgb(Image.new("RGB", (512, 512), "white")) )
        self.assertEquals( (0.0, 0.0, 0.0),
                           deflicker.average_rgb(Image.new("RGB", (512, 512), "black")) )
        self.assertEquals( (255.0, 0.0, 0.0),
                           deflicker.average_rgb(Image.new("RGB", (512, 512), "red")) )
        
        # random image should be average grey
        W, H = 185, 103
        image = Image.new("RGB", (W, H), "black")
        for x in range(W):
            for y in range(H):
                image.putpixel( (x,y), (randint(0, 255), randint(0, 255), randint(0, 255)) )
        
        average = deflicker.average_rgb(image)
        for val in average:
            self.assertGreater(val, 126)
            self.assertLess(val, 129)
            
    def test_luminance(self):
        self.assertGreater(deflicker.luminance( (255.,255.,255.) ), 254.9 )
        self.assertLess(deflicker.luminance( (0.,0.,0.) ), 0.1 )
        self.assertEquals(deflicker.luminance( (125., 45., 12.) ), 71.93533206985285)

