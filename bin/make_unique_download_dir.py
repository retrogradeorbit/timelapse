#!/usr/bin/env python

"""Make a unique directory to store the photos that you are downloading from the camera"""

from datetime import datetime
import os
import re
import sys

def get_path(path='download', now=None):
    now = now or datetime.now()
    dirname = str(now).replace(' ','-')
    return advance_path(os.path.join(path, dirname))

def advance_path(path):
    if os.path.exists(path):
        if re.search(r'\-\d+$', path):
            # advance the digit
            base, digits = path.rsplit('-',1)
            digits = int(digits[-1]) + 1
            return advance_path("{0}-{1}".format(base,digits))
        
        # add a version digit
        return advance_path("{0}-1".format(path))

    # doesn't exist
    return path

def main():
    path = sys.argv[1] if len(sys.argv)>1 else 'download'
    print get_path(path)

if __name__ == "__main__":
    main()
