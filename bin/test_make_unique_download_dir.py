import unittest2 as unittest

import make_unique_download_dir as test_module
import os
import shutil
from datetime import datetime

TMP = "/tmp/test_chdk_downloads"

class Mkdir(object):
    """temp directory context manager"""
    def __init__(self, path):
        self.path = path

    def __enter__(self):
        os.makedirs(self.path)
        return self

    def __exit__(self, *args, **kwargs):
        shutil.rmtree(self.path)

    def path(self, path):
        return os.path.join(self.path, path)

class MakeUniqueDownloadTestSuite(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_path(self):
        now = datetime.now()
        result = test_module.get_path(TMP, now=now)
        self.assertTrue(result.startswith(TMP))

        with Mkdir(result):
            # we should get here without exception
            # try and make another one with the same timestamp
            result2 = test_module.get_path(TMP, now=now)
            self.assertEquals(result2, "{0}-1".format(result))

            with Mkdir(result2):
                result3 = test_module.get_path(TMP, now=now)
                self.assertEquals(result3, result+"-2")

