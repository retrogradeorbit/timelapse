-- Timelapse

--[[
@title Time Lapse Photos (OQ 4)
@param a Interval (secs)
@default a 10
@param b +Interval (1/100 sec) 
@default b 0
]]

wait = a*1000 + b*100
shot_num = 0

print("Time Lapse Photos")
while not is_key("set") do
   tick = get_tick_count()
   shot_num = shot_num + 1
   print("Shot:",shot_num)
   shoot()

   -- prevent live feed. less power/delays
   click("display")

   -- Wait 'wait' msecs, including the
   -- time it takes to shoot. 
   -- Shoots as fast as possible if
   -- each shot takes longer than
   -- 'wait' msecs. 
   delay = wait - (get_tick_count() - tick)
   if( delay>0 ) then
      sleep(dealy)
   end
end
