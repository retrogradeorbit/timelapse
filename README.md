Timelapse
=========

My support scripts for shooting and generating timelapse video using Canon PowerShot/IXUS cameras which support the CHDK firmware.

Requirements
------------
 - Linux
 - mencoder
 - ffmpeg
 - USB sdcard reader/writer
 - Canon camera that supports CHDK

Usage
-----
The scripts manage the the SDCard, its firmware, files and their transfer. Each action is composed of a make command.

Example
-------
[Example video](https://vimeo.com/71152376) made with these scripts.

Mounting
--------

```
$ make mount
```
Mounts /dev/sde1 by default (where my sdcard appears). You can specify an alternate device for example:

```
$ make mount DEVICE=/dev/sdd1
```

Setting Up Firmware
-------------------
Once the card is mounted, copy the firmware on. You will need to fetch your firmware to put in the firmware directory. The default is PowerShot A2300:

```
$ make upload-scripts
```

You can pass scripts a firmware path with the suffix FIRMWARE. Like 'make scripts FIRMWARE=firmware/chdk-....'

Now unmount the card...

```
$ make umount
```

...and go start the timelapse script. (TODO: describe this)

Downloading The Shoot
---------------------
Plug in the card, mount the filesystem and issue a download

```
$ make mount
$ make download
```
This will now set the 'active project' to this project. From here on you can work on this set of images with the following commands:

Resizing
--------

```
$ make resize
```

Joining into Movie
------------------

```
$ make join
```

The movie file will be created in the download directory as 'output-normal.avi'

Deflickering
------------
With the images downloaded and resized, go:

```
$ make deflicker
```

This could take some time depending apon the length of shooting.

After its complete, join it together:

```
$ make join-deflickered
```

To create output-deflickered.avi

